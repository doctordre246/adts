#include <iostream>
#include "List.h"

using namespace std;

int main()
{

 List L1, L2;
 L1.insertAt(1,1);
 L1.insertAt(2,2);
 L1.insertAt(3,3);
 L1.insertAt(1,1);
 L1.insertAt(3,1);
 L1.insertAt(2,4);
 L1.insertAt(6,2);
 L1.insertAt(5,5);
 L1.insertAt(3,6);
 
 
	
	cout << L1.getAt(1)<<"\n"<<endl<<endl<<endl;
	cout << L1.getAt(2)<<"\n"<<endl<<endl<<endl;
	cout << L1.getAt(3)<<"\n"<<endl<<endl<<endl;
	cout << L1.getAt(4)<<"\n"<<endl<<endl<<endl;
	cout << L1.getAt(5)<<"\n"<<endl<<endl<<endl;
	cout << L1.getAt(6)<<"\n"<<endl<<endl<<endl;
	cout << L1.getAt(7)<<"\n"<<endl<<endl<<endl;
	cout << L1.getAt(8)<<"\n"<<endl<<endl<<endl;
	cout << L1.getAt(9)<<"\n"<<endl<<endl<<endl;
	
	cout<<L1.size()<<endl<<endl<<endl;
	L1.removeAt(7);
	cout<<L1.size()<<endl<<endl<<endl;
	L1.clear();
	cout<<L1.size()<<endl<<endl<<endl;


 //Do some stuff with L1, L2, ...
 // ...

}
